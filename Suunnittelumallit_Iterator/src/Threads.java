import java.util.Iterator;

public class Threads extends Thread{
	
	private Iterator<String> iterator;
	private String str = "";
	
	public Threads(Iterator<String> iterator) {
		this.iterator = iterator;
	}
	
	public void run() {
		while (iterator.hasNext()) {
            System.out.println(iterator.next());
            str = (String) iterator.next();
            
            if (str.equals("neljä")) {
                iterator.remove();
                System.out.println("\nThe element neljä is removed");
                break;
             }
		}
		
	}

}
