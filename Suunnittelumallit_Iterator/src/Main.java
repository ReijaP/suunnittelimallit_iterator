import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Main extends Thread{

	public static void main(String[] args) throws InterruptedException{
		List<String> iterableList = new LinkedList<String>();
		
		iterableList.add("yksi");
		iterableList.add("kaksi");
		iterableList.add("kolme");
		iterableList.add("neljä");
		
		/*Iterator<String> iterator1 = iterableList.iterator();
		Iterator<String> iterator2 = iterableList.iterator();
		
		Threads thread1 = new Threads(iterator1);
		Threads thread2 = new Threads(iterator2);
		
		thread1.start();
		thread2.start();*/
		
		Iterator<String> iterator = iterableList.iterator();
		
		Threads thread3 = new Threads(iterator);
		Threads thread4 = new Threads(iterator);
		
		thread3.start();
		thread3.join();
		//iterableList.add("viisi");
		thread4.start();
		thread4.join();
		
		System.out.println("\nThe LinkedList elements are: ");
	      for (String s: iterableList) {
	         System.out.println(s);
	      }

	}
}
